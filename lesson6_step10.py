#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import time

from selenium import webdriver
from selenium.webdriver.common.by import By


if __name__ == '__main__':
    browser = webdriver.Chrome()

    try:
        link1 = 'http://suninjuly.github.io/registration1.html'
        link2 = 'http://suninjuly.github.io/registration2.html'
        browser.get(link1)

        name_field = browser.find_element(By.CSS_SELECTOR, '.form-control.first[required]')
        name_field.send_keys('NAME')

        lastname_field = browser.find_element(By.CSS_SELECTOR, '.form-control.second[required]')
        lastname_field.send_keys('LASTNAME')

        email_field = browser.find_element(By.CSS_SELECTOR, '.form-control.third[required]')
        email_field.send_keys('EMAIL')

        button = browser.find_element(By.CSS_SELECTOR, 'button.btn')
        button.click()

        time.sleep(1)

        welcome_text_elt = browser.find_element(By.TAG_NAME, 'h1')
        welcome_text = welcome_text_elt.text

        expected_msg = 'Congratulations! You have successfully registered!'
        assert expected_msg == welcome_text, \
            f'Expected message: "{expected_msg}", actual: "{welcome_text}"'

    finally:

        time.sleep(10)
        browser.quit()
